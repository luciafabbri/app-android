package com.example.androidapp.Database;

import androidx.lifecycle.LiveData;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import androidx.room.Dao;


import com.example.androidapp.Entities.CardItem;

import java.util.List;

@Dao
public interface CardItemDAO {
    // The selected on conflict strategy ignores a new CardItem
    // if it's exactly the same as one already in the list.
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void addCardItem(CardItem cardItem);

    @Transaction
    @Query("SELECT * from event ORDER BY event_id DESC")
    LiveData<List<CardItem>> getCardItems();

    @Query("SELECT event_title FROM event WHERE event_id IN (:id)")
    LiveData<List<String>> getEventById(List<Integer> id);


}
