package com.example.androidapp.Database;

import android.content.Context;

import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.androidapp.Entities.CardItem;
import com.example.androidapp.Entities.FavouriteList;
import com.example.androidapp.Entities.PartecipationList;
import com.example.androidapp.Entities.UserItem;

import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;

@androidx.room.Database(entities = {CardItem.class, UserItem.class, FavouriteList.class, PartecipationList.class}, version = 3)
public abstract class Database extends RoomDatabase {
    public abstract CardItemDAO cardItemDAO();
    public abstract UserDAO userDAO();
    public abstract FavouriteDAO favouriteDAO();
    public abstract PartecipationDAO partecipationDAO();


    private static volatile Database INSTANCE;
    private static final int NUMBER_OF_THREADS = 4;
    static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    static Database getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (Database.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            Database.class, "events_database")
                            .build();
                }
            }
        }
        return INSTANCE;
    }
}
