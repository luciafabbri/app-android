package com.example.androidapp.Database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.androidapp.Entities.FavouriteList;

import java.util.List;

@Dao
public interface FavouriteDAO {
        @Insert(onConflict = OnConflictStrategy.IGNORE)
        void addFavourite(FavouriteList pair);

        @Query("SELECT DISTINCT * FROM favourite")
        LiveData<List<FavouriteList>> getList();

        @Query("SELECT DISTINCT * FROM favourite WHERE event_id = :event AND user_id = :user")
        LiveData<FavouriteList> getFavourite(int event, int user);

        @Query("SELECT DISTINCT event_id FROM favourite WHERE user_id = :id")
        LiveData<List<Integer>> getByUser (int id);

        @Query("DELETE FROM favourite WHERE event_id = :event AND user_id = :user")
        void deleteFavourite(int event, int user);
}
