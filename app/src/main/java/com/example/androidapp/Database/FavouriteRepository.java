package com.example.androidapp.Database;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.example.androidapp.Entities.FavouriteList;

import java.util.List;

public class FavouriteRepository {
    private FavouriteDAO favouriteDAO;
    private LiveData<List<FavouriteList>> favouriteList;

    public FavouriteRepository(Application application) {
        Database db = Database.getDatabase(application);
        favouriteDAO = db.favouriteDAO();
        favouriteList = favouriteDAO.getList();
    }

    public void addFavourite (final FavouriteList favourite) {
        Database.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                favouriteDAO.addFavourite(favourite);
            }
        });
    }

    public void deleteFavourite (int event, int user) {
        Database.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                favouriteDAO.deleteFavourite(event, user);
            }
        });
    }

    public LiveData<FavouriteList> getFavourite(int event, int user) {
        return favouriteDAO.getFavourite(event, user);
    }

    public LiveData<List<FavouriteList>> getFavouriteList() {
        return favouriteList;
    }

    public LiveData<List<Integer>> userFavourite(int id) { return favouriteDAO.getByUser(id); }
}
