package com.example.androidapp.Database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import com.example.androidapp.Entities.PartecipationList;

import java.util.List;

@Dao
public interface PartecipationDAO {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void addPartecipation(PartecipationList pair);

    @Query("SELECT DISTINCT * FROM partecipation WHERE event_id = :event AND user_id = :user")
    LiveData<PartecipationList> getPartecipation(int event, int user);

    @Query("SELECT * FROM partecipation")
    LiveData<List<PartecipationList>> getList();

    @Query("SELECT DISTINCT event_id FROM partecipation WHERE user_id = :id")
    LiveData<List<Integer>> getByUser (int id);

    @Query("DELETE FROM partecipation WHERE user_id = :user AND event_id = :event")
    void deletePartecipation(int event, int user);
}
