package com.example.androidapp.Database;

import android.app.Application;

import androidx.lifecycle.LiveData;
import com.example.androidapp.Entities.PartecipationList;

import java.util.List;

public class PartecipationRepository {
    private PartecipationDAO partecipationDAO;
    private LiveData<List<PartecipationList>> partecipationList;

    public PartecipationRepository(Application application) {
        Database db = Database.getDatabase(application);
        partecipationDAO = db.partecipationDAO();
        partecipationList = partecipationDAO.getList();
    }

    public void addPartecipation (final PartecipationList partecipation) {
        Database.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                partecipationDAO.addPartecipation(partecipation);
            }
        });
    }

    public void deletePartecipation (int event, int user) {
        Database.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                partecipationDAO.deletePartecipation(event, user);
            }
        });
    }

    public LiveData<PartecipationList> getPartecipation(int event, int user) {
        return partecipationDAO.getPartecipation(event, user);
    }

    public LiveData<List<PartecipationList>> getPartecipationList() {
        return partecipationList;
    }

    public LiveData<List<Integer>> userPartecipation(int id) { return partecipationDAO.getByUser(id); }
}
