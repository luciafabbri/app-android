package com.example.androidapp.Database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import com.example.androidapp.Entities.UserItem;
import java.util.List;

@Dao
public interface UserDAO {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void addUser(UserItem user);

    @Delete
    void deleteUser (UserItem user);

    @Transaction
    @Query("SELECT * from user ORDER BY user_id DESC")
    LiveData<List<UserItem>> getUsers();

    @Update
    void modifyUser (UserItem userItem);

    @Query("SELECT * FROM user WHERE user_email = :email AND user_psw = :password")
    LiveData<UserItem> getUser(String email, String password);

    @Query("SELECT * FROM user WHERE user_id = :id")
    LiveData<UserItem> getUserById(int id);
}
