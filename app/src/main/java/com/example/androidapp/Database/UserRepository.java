package com.example.androidapp.Database;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.example.androidapp.Entities.UserItem;

import java.util.List;

public class UserRepository {
    private UserDAO userDAO;
    private LiveData<List<UserItem>> userList;
    private LiveData<UserItem> user;

    public UserRepository(Application application) {
        Database db = Database.getDatabase(application);
        userDAO = db.userDAO();
        userList = userDAO.getUsers();
    }

    // Room executes all queries on a separate thread.
    // Observed LiveData will notify the observer when the data has changed.
    public LiveData<List<UserItem>> getUsers(){
        return userList;
    }

    // You must call this on a non-UI thread or your app will throw an exception. Room ensures
    // that you're not doing any long running operations on the main thread, blocking the UI.
    public void addUser (final UserItem userItem) {
        Database.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                userDAO.addUser(userItem);
            }
        });
    }

    public void deleteUser (UserItem userItem) {
        Database.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                userDAO.deleteUser(userItem);
            }
        });
    }

    public void modifyUser (UserItem userItem) {

    }

    public LiveData<UserItem> getUser(String email, String password) {
        return userDAO.getUser(email, password);

    }

    public LiveData<UserItem> getUserById(int id) {
        return userDAO.getUserById(id);
    }
}
