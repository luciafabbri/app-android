package com.example.androidapp;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;

import com.example.androidapp.Entities.CardItem;
import com.example.androidapp.Entities.FavouriteList;
import com.example.androidapp.Entities.PartecipationList;
import com.example.androidapp.ViewModel.EventViewModel;
import com.example.androidapp.ViewModel.ListViewModel;

public class DetailsFragment extends Fragment {

    private TextView titleTextView;
    private TextView dateTextView;
    private ImageView eventImageView;
    private TextView descriptionTextView;
    private TextView cityTextView;
    private ImageButton favourite;
    private ImageButton partecipation;
    private ImageButton favourite1;
    private ImageButton partecipation1;

    private EventViewModel eventViewModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.event_details, container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        descriptionTextView = view.findViewById(R.id.descriptionTextView);
        titleTextView = view.findViewById(R.id.eventNameTextView);
        dateTextView = view.findViewById(R.id.dateTextView);
        eventImageView = view.findViewById(R.id.eventImage);
        cityTextView = view.findViewById(R.id.cityTextView);

        Activity activity = getActivity();
        if (activity != null) {
            Utilities.setUpToolbar((AppCompatActivity) activity, getString(R.string.app_name));

            ListViewModel listViewModel = new ViewModelProvider((ViewModelStoreOwner) activity).get(ListViewModel.class);
            favourite = view.findViewById(R.id.buttonFavorites);
            partecipation = view.findViewById(R.id.buttonPartecipation);
            favourite1 = view.findViewById(R.id.buttonFavorites1);
            partecipation1 = view.findViewById(R.id.buttonPartecipation1);

            eventViewModel = new ViewModelProvider((ViewModelStoreOwner) activity).get(EventViewModel.class);

            listViewModel.getSelected().observe(getViewLifecycleOwner(), new Observer<CardItem>() {
                @Override
                public void onChanged(CardItem cardItem) {

                    dateTextView.setText(cardItem.getDate());
                    titleTextView.setText(cardItem.getTitle());
                    descriptionTextView.setText(cardItem.getDescription());
                    cityTextView.setText(cardItem.getCity());

                    String imagePath = cardItem.getImageResource();
                        Drawable drawable = ContextCompat.getDrawable(activity,
                                activity.getResources().getIdentifier(imagePath, "drawable", activity.getPackageName()));
                        eventImageView.setImageDrawable(drawable);

                    eventViewModel.findPartecipation(cardItem.getId(), Utilities.getID()).observe(getViewLifecycleOwner(), new Observer<PartecipationList>() {
                        @Override
                        public void onChanged(PartecipationList partecipationList) {
                            Log.d("porcodio",String.valueOf((partecipationList != null)));
                            if (partecipationList != null) {
                                Log.d("porcodio", "part");
                                partecipation.setVisibility(View.INVISIBLE);
                                partecipation1.setVisibility(View.VISIBLE);
                            } else {
                                partecipation1.setVisibility(View.INVISIBLE);
                                partecipation.setVisibility(View.VISIBLE);
                            }
                        }
                    });

                    eventViewModel.findFavourite(cardItem.getId(), Utilities.getID()).observe(getViewLifecycleOwner(), new Observer<FavouriteList>() {
                        @Override
                        public void onChanged(FavouriteList list) {
                            Log.d("porcodio",String.valueOf((list != null)));
                            if (list != null) {
                                Log.d("porcodio", "fav");
                                favourite.setVisibility(View.INVISIBLE);
                                favourite1.setVisibility(View.VISIBLE);
                            } else {
                                favourite1.setVisibility(View.INVISIBLE);
                                favourite.setVisibility(View.VISIBLE);
                            }
                        }
                    });

                    favourite.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            eventViewModel.addFavourite(new FavouriteList(Utilities.getID(), cardItem.getId()));
                            Toast.makeText(activity, "Aggiunto alla lista Preferiti", Toast.LENGTH_SHORT).show();
                            favourite.setVisibility(View.INVISIBLE);
                            favourite1.setVisibility(View.VISIBLE);
                        }
                    });


                    favourite1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            eventViewModel.deleteFavourite(cardItem.getId(), Utilities.getID());
                            Toast.makeText(activity, "Rimosso dalla lista dei Preferiti", Toast.LENGTH_SHORT).show();
                            favourite1.setVisibility(View.INVISIBLE);
                            favourite.setVisibility(View.VISIBLE);
                        }
                    });

                    partecipation.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            eventViewModel.addPartecipation(new PartecipationList(Utilities.getID(), cardItem.getId()));
                            Toast.makeText(activity, "Aggiunto alla lista Partecipazioni", Toast.LENGTH_SHORT).show();
                            partecipation.setVisibility(View.INVISIBLE);
                            partecipation1.setVisibility(View.VISIBLE);
                        }
                    });

                    partecipation1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            eventViewModel.deletePartecipation(cardItem.getId(), Utilities.getID());
                            Toast.makeText(activity, "Rimosso dalla lista Partecipazioni", Toast.LENGTH_SHORT).show();
                            partecipation1.setVisibility(View.INVISIBLE);
                            partecipation.setVisibility(View.VISIBLE);
                        }
                    });

            view.findViewById(R.id.shareButton).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent sendIntent = new Intent(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, v.getContext().getString(R.string.title) + ": " + titleTextView.getText().toString()
                            + "\n" + v.getContext().getString(R.string.date) + ": " + dateTextView.getText().toString()
                            + "\n" + v.getContext().getString(R.string.city) + ": " + cityTextView.getText().toString()
                            + "\n" + v.getContext().getString(R.string.descrizione) + ": " + descriptionTextView.getText().toString());
                    sendIntent.setType("text/plain");
                    if (v.getContext() != null &&
                            sendIntent.resolveActivity(v.getContext().getPackageManager()) != null) {
                        v.getContext().startActivity(Intent.createChooser(sendIntent, null));
                    }
                }
            });
         }
        });
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.findItem(R.id.app_bar_search).setVisible(false);

    }
}
