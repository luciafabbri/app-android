package com.example.androidapp.Entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName="event")
public class CardItem {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "event_id")
    private int id;
    @ColumnInfo(name = "event_title")
    private String title;
    @ColumnInfo(name = "event_description")
    private String description;
    @ColumnInfo(name = "event_date")
    private String date;
    @ColumnInfo(name = "event_image")
    private String imageResource;
    @ColumnInfo(name = "event_city")
    private String city;


    public CardItem(String title, String description, String date, String imageResource, String city) {
        this.title = title;
        this.description = description;
        this.date = date;
        this.imageResource = imageResource;
        this.city = city;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getDate() {
        return date;
    }


    public String getImageResource() { return imageResource; }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }
}
