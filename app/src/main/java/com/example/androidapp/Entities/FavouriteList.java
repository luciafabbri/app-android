package com.example.androidapp.Entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "favourite")
public class FavouriteList {
    @PrimaryKey(autoGenerate = true)
    private int id;
    @ColumnInfo(name = "event_id")
    private int eventID;
    @ColumnInfo(name = "user_id")
    private int userID;

    public FavouriteList(int userID, int eventID) {
        this.eventID = eventID;
        this.userID = userID;
    }

    public int getEventID() {
        return eventID;
    }

    public int getUserID() {
        return userID;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
/*
public class UserFavourites {
    @Embedded public UserItem user;
    @Relation(
            parentColumn = "user_id",
            entityColumn = "event_id",
            associateBy = @Junction(UserEventCrossRef.class)
    )
    public List<CardItem> list;
}

 */
