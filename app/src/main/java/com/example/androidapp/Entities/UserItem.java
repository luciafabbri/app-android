package com.example.androidapp.Entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "user")
public class UserItem {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "user_id")
    private int id;
    @ColumnInfo(name="user_name")
    private String name;
    @ColumnInfo(name = "user_surname")
    private String surname;
    @ColumnInfo(name = "user_email")
    private String email;
    @ColumnInfo (name = "user_psw")
    private String password;
    @ColumnInfo (name = "user_image")
    private String imageResource;

    public UserItem(String name, String surname, String email, String password, String imageResource) {
        this.email = email;
        this.name = name;
        this.surname = surname;
        this.password = password;
        this.imageResource = imageResource;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getImageResource() {
        return imageResource;
    }

}
