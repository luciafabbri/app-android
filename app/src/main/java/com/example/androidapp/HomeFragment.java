package com.example.androidapp;

import androidx.fragment.app.Fragment;

import android.app.Activity;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;

import com.example.androidapp.Entities.UserItem;
import com.example.androidapp.ViewModel.AddViewModel;
import com.example.androidapp.ViewModel.EventViewModel;
import com.example.androidapp.ViewModel.ListViewModel;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.List;


public class HomeFragment extends Fragment {

    private static final String LOG = "Home-Fragment_Project";
    private AddViewModel addViewModel;
    private EventViewModel eventViewModel;
    private ListView favouriteListView;
    private ListView partecipationListView;
    private ListViewModel listViewModel;
    private TextView nameTextView;
    private TextView surnameTextView;
    CircularImageView imageView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    /**
     * Called to have the fragment instantiate its user interface view.
     *
     * @param inflater           The LayoutInflater object that can be used to inflate any views in the fragment,
     * @param container          If non-null, this is the parent view that the fragment's UI should be attached to.
     * @param savedInstanceState If non-null, this fragment is being re-constructed from a previous saved state as given here.
     * @return Return the View for the fragment's UI, or null.
     */
    @Nullable
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final Activity activity = getActivity();
        if (activity != null) {
            Utilities.setUpToolbar((AppCompatActivity) getActivity(), getString(R.string.app_name));

            favouriteListView = view.findViewById(R.id.favouriteListView);
            partecipationListView = view.findViewById(R.id.partecipationListView);
            nameTextView = view.findViewById(R.id.nameTextView);
            surnameTextView = view.findViewById(R.id.surnameTextView);
            favouriteListView.setClickable(false);
            partecipationListView.setClickable(false);
            imageView = view.findViewById(R.id.userImage);

            addViewModel = new ViewModelProvider((ViewModelStoreOwner) activity).get(AddViewModel.class);
            eventViewModel = new ViewModelProvider((ViewModelStoreOwner) activity).get(EventViewModel.class);
            listViewModel = new ViewModelProvider((ViewModelStoreOwner) activity).get(ListViewModel.class);
            addViewModel.getUserById(Utilities.getID()).observe(getViewLifecycleOwner(), new Observer<UserItem>() {
                @Override
                public void onChanged(UserItem userItem) {

                    nameTextView.setText(userItem.getName());
                    surnameTextView.setText(userItem.getSurname());
                    if (!userItem.getImageResource().equals("")) {
                        imageView.setImageBitmap(Utilities.getImageBitmap(activity, Uri.parse(userItem.getImageResource())));
                    }

                    eventViewModel.userFavourite(userItem.getId()).observe(getViewLifecycleOwner(), new Observer<List<Integer>>() {
                        @Override
                        public void onChanged(List<Integer> item) {
                            listViewModel.getEventList(item).observe(getViewLifecycleOwner(), new Observer<List<String>>() {
                                @Override
                                public void onChanged(List<String> strings) {
                                    ArrayAdapter<String> adapter = new ArrayAdapter<>(activity, R.layout.support_simple_spinner_dropdown_item, strings);
                                    favouriteListView.setAdapter(adapter);
                                    expandListViewHeight(favouriteListView);
                                }
                            });
                        }
                    });

                    eventViewModel.userPartecipation(userItem.getId()).observe(getViewLifecycleOwner(), new Observer<List<Integer>>() {
                        @Override
                        public void onChanged(List<Integer> item) {
                            listViewModel.getEventList(item).observe(getViewLifecycleOwner(), new Observer<List<String>>() {
                                @Override
                                public void onChanged(List<String> strings) {
                                    ArrayAdapter<String> adapter = new ArrayAdapter<>(activity, R.layout.support_simple_spinner_dropdown_item, strings);
                                    partecipationListView.setAdapter(adapter);
                                    expandListViewHeight(partecipationListView);
                                }
                            });
                        }
                    });

                }
            });

        } else {
            Log.e(LOG, "Activity is null");
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.findItem(R.id.app_bar_search).setVisible(false);

    }

    public static void expandListViewHeight(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        listView.measure(0, 0);
        params.height = listView.getMeasuredHeight() * listAdapter.getCount() +
                (listView.getDividerHeight() * (listAdapter.getCount() - 1)) + listView.getMeasuredHeight();
        listView.setLayoutParams(params);
    }

}

