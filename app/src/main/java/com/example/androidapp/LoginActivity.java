package com.example.androidapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.androidapp.Entities.UserItem;
import com.example.androidapp.ViewModel.AddViewModel;

public class LoginActivity extends AppCompatActivity {
    EditText editTextEmail, editTextPassword;
    Button buttonLogin;
    TextView textViewRegister;
    ImageView imageViewLogo;
    private AddViewModel addView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_page);

        editTextEmail = findViewById(R.id.emailEditText);
        editTextPassword = findViewById(R.id.passwordEditText);
        buttonLogin = findViewById(R.id.btnlogin);
        imageViewLogo = findViewById(R.id.logo);
        textViewRegister = findViewById(R.id.registerTextView);

        textViewRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
            }
        });

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = editTextEmail.getText().toString().trim();
                String password = editTextPassword.getText().toString().trim();

                addView = new ViewModelProvider(LoginActivity.this).get(AddViewModel.class);
                addView.getUser(email, password).observe(LoginActivity.this, new Observer<UserItem>() {
                    @Override
                    public void onChanged(UserItem userItem) {
                        if (userItem != null) {
                            Intent i = new Intent(LoginActivity.this, MainActivity.class);
                            i.putExtra("userid", userItem.getId());
                            startActivity(i);
                            finish();
                        } else {
                            Toast.makeText(LoginActivity.this, getString(R.string.login_error),
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                });


            }
        });

    }
}
