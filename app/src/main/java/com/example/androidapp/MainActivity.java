package com.example.androidapp;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import android.app.Activity;

import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.example.androidapp.Database.CardItemRepository;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.mapbox.geojson.Point;
import com.mapbox.search.MapboxSearchSdk;
import com.mapbox.search.ResponseInfo;
import com.mapbox.search.ReverseGeoOptions;
import com.mapbox.search.ReverseGeocodingSearchEngine;
import com.mapbox.search.SearchCallback;
import com.mapbox.search.SearchRequestTask;
import com.mapbox.search.location.LocationProvider;
import com.mapbox.search.result.SearchResult;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import timber.log.Timber;

public class MainActivity extends AppCompatActivity {

    private static final String FRAGMENT_TAG_HOME = "HomeFragment";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            Utilities.insertFragment(this, new HomeFragment(), FRAGMENT_TAG_HOME);
            Bundle extras = getIntent().getExtras();
            int userId;
            if (extras != null) {
                //salvo id utente così posso recuperarlo in qualsiasi fragment
                userId = extras.getInt("userid");
                Utilities.setID(userId);
            }
        }

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
                switch (item.getItemId()) {
                    case R.id.mapItem:
                        Intent intent = new Intent(MainActivity.this, MapActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.listItem:
                        if (f instanceof GalleryFragment) {}
                        else {
                            Utilities.insertFragment(MainActivity.this, new GalleryFragment(), "Gallery Fragment");
                        }

                        break;
                    case R.id.userItem:
                        if (f instanceof HomeFragment) {}
                        else {
                            Utilities.insertFragment(MainActivity.this, new HomeFragment(), "Home Fragment");
                        }
                        break;
                }
                return true;
            }
        });

        bottomNavigationView.setSelectedItemId(R.id.userItem);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.top_app_bar, menu);
        return true;
    }

}