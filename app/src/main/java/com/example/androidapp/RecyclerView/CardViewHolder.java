package com.example.androidapp.RecyclerView;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.androidapp.R;

public class CardViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    ImageView imageCardView;
    TextView titleTextView;

    private OnItemListener itemListener;

    CardViewHolder(@NonNull View itemView, OnItemListener listener) {
        super(itemView);
        imageCardView = itemView.findViewById(R.id.eventImage);
        titleTextView = itemView.findViewById(R.id.eventName);
        itemListener = listener;

        itemView.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        itemListener.onItemClick(getAbsoluteAdapterPosition());
    }
}
