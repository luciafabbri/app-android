package com.example.androidapp.RecyclerView;

public interface OnItemListener {
    void onItemClick(int position);
}
