package com.example.androidapp.ViewModel;

import android.app.Application;
import android.graphics.Bitmap;
import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.androidapp.Database.UserRepository;
import com.example.androidapp.Entities.UserItem;

import java.util.List;


//AndroidViewModel ha riferimento al contest, cosa che ViewModel semplice non ha
public class AddViewModel extends AndroidViewModel {

    private final MutableLiveData<Bitmap> imageBitmap = new MutableLiveData<>();
    private UserRepository repository;
    private LiveData<List<UserItem>> userList;

    public AddViewModel(@NonNull Application application) {
        super(application);
        repository = new UserRepository(application);
        userList = repository.getUsers();
    }

    public LiveData<List<UserItem>> getUsers() { return userList; }

    public void setImageBitmap (Bitmap bitmap) {
        imageBitmap.setValue(bitmap);
    }

    public LiveData<Bitmap> getBitmap() {
        return imageBitmap;
    }

    public void addUser (UserItem item) { repository.addUser (item); }

    public LiveData<UserItem> getUser(String email, String password) { return repository.getUser(email, password); }

    public LiveData<UserItem> getUserById(int id) { return repository.getUserById(id); }

}
