package com.example.androidapp.ViewModel;

import android.app.Application;
import android.graphics.Bitmap;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.androidapp.Database.FavouriteRepository;
import com.example.androidapp.Database.PartecipationRepository;
import com.example.androidapp.Entities.FavouriteList;
import com.example.androidapp.Entities.PartecipationList;

import java.util.List;

public class EventViewModel extends AndroidViewModel {

    private final MutableLiveData<Bitmap> imageBitmap = new MutableLiveData<>();
    private FavouriteRepository favouriteRepository;
    private PartecipationRepository partecipationRepository;
    private LiveData<List<FavouriteList>> favourite;
    private LiveData<List<PartecipationList>> partecipation;

    public EventViewModel(@NonNull Application application) {
        super(application);
        favouriteRepository = new FavouriteRepository(application);
        favourite = favouriteRepository.getFavouriteList();
        partecipationRepository = new PartecipationRepository(application);
        partecipation = partecipationRepository.getPartecipationList();
    }

    public LiveData<List<FavouriteList>> getFavourite() { return favourite; }

    public LiveData<List<PartecipationList>> getPartecipation() { return partecipation; }

    public LiveData<FavouriteList> findFavourite(int event, int user) { return favouriteRepository.getFavourite(event, user);   }

    public void addFavourite (FavouriteList item) { favouriteRepository.addFavourite (item); }

    public LiveData<List<Integer>> userFavourite(int id) { return favouriteRepository.userFavourite(id); }

    public LiveData<PartecipationList> findPartecipation(int event, int user) { return partecipationRepository.getPartecipation(event, user); }

    public void addPartecipation(PartecipationList item) { partecipationRepository.addPartecipation(item); }

    public LiveData<List<Integer>> userPartecipation(int id) { return partecipationRepository.userPartecipation(id); }

    public void deleteFavourite(int event, int user) { favouriteRepository.deleteFavourite(event, user); }

    public void deletePartecipation(int event, int user) { partecipationRepository.deletePartecipation(event, user); }

}
